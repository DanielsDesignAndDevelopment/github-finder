import React, { useContext } from 'react';
import UserItem from './UserItem';
import Spinner from '../layout/Spinner';
import GithubContext from '../../context/github/githubContext';

function Users() {
  const githubContext = useContext(GithubContext);
  const { loading, users } = githubContext;
  const viewPort = window.innerWidth;

  if (loading) {
    return <Spinner />;
  } else {
    return (
      <div style={userStyle(viewPort)}>
        {users.map((user) => (
          <UserItem key={user.id} user={user} />
        ))}
      </div>
    );
  }
}

const userStyle = (viewPortWidth) => ({
  display: 'grid',
  gridTemplateColumns: viewPortWidth < 584 ? 'repeat(2, 1fr)' : 'repeat(3, 1fr)',
  gridGap: '1rem',
});

export default Users;