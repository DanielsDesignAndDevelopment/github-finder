import React, { Fragment } from 'react';
import Search from '../users/Search';
import NavLinks from '../repos/NavLinks';
import Users from '../users/Users';

function Home() {
  return (
    <Fragment>
      <NavLinks />
      <Search />
      <Users />
    </Fragment>
  );
}

export default Home;