import React from 'react';
import { FaBitbucket } from 'react-icons/fa';

function NavLinks () {
  return (
    <div className="text-center">
      <a 
        href="https://bitbucket.org/DanielsDesignAndDevelopment/github-finder"
        style={linkStyle}       
      >
        <FaBitbucket style={iconStyle} /> See code
      </a>
      <span className="text-danger">|</span>
      <a href="https://danielsdnd.com" style={linkStyle}>
        Back to danielsdnd.com
      </a>
    </div>
  )
}

const linkStyle = {
  textDecoration: "none",
  margin: "10px"
}

const iconStyle = {
  position: "relative",
  top: "2px"
}

export default NavLinks;